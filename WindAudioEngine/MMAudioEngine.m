//
//  MMAudioEngine.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMAudioEngine.h"
#import "MMPlayer.h"
#import "MMQueuePlayer.h"
#import "MMPlayerItem.h"

@interface MMAudioEngine() < MMPlayerDelegate, MMQueuePlayerDelegate >

@property (nonatomic, strong) NSArray< MMPlayer *> *synchroniousPlayersQueue;

@end

@implementation MMAudioEngine

#pragma mark - Initializers

+ (instancetype)sharedInstance {
    static MMAudioEngine* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[MMAudioEngine alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _activePlayers = @[];
    }
    return self;
}

#pragma mark - Class Methods

- (void)startPlaybackForPlayerItem:(MMPlayerItem *)playerItem {
    MMPlayer *player = [[MMPlayer alloc] initWithPlayerItem:playerItem];
    if (player) {
        _activePlayers = [_activePlayers arrayByAddingObject:player];
        player.delegate = self;
        [player play];
    }
}

- (void)startPlaybackForPlaylistWithItems:(NSArray<MMPlayerItem *> *)playerItems {
    MMQueuePlayer* queuePlayer = [[MMQueuePlayer alloc] initWithPlayerItems:playerItems];
    if (queuePlayer) {
        _activePlayers = [_activePlayers arrayByAddingObject:queuePlayer];
        queuePlayer.delegate = self;
        [queuePlayer play];
    }
}

- (id)playerWithItem:(MMPlayerItem *)playerItem {
  for (id <MMPlayerProtocol> player in _activePlayers) {
    if ([player isMemberOfClass:[MMPlayer class]]) {
      if ([[(MMPlayer *)player playerItem] isEqualToPlayerItem:playerItem]) {
        return player;
      }
    }
  }
  return nil;
}

- (void)switchActivePlayerItem:(MMPlayerItem *)currentlyPlayingItem
                      withItem:(MMPlayerItem *)playerItem {
  MMPlayer* currentPlayer = [self playerWithItem:currentlyPlayingItem];
  [currentPlayer stop];
  
  [self startPlaybackForPlayerItem:playerItem];
}

#pragma mark - MMPlayerDelegate

- (void)playerDidReachEndOfPlayback:(MMPlayer *)player {
    [self removePlayerFromActivePlayers:player];
}

#pragma mark - MMQueuePlayerDelegate

- (void)queuePlayerDidReachEndOfPlayback:(MMQueuePlayer *)player {
    [self removePlayerFromActivePlayers:player];
}

#pragma mark - Helpers

- (void)removePlayerFromActivePlayers:(id)player {
    NSInteger playerIndex = NSNotFound;
    playerIndex = [_activePlayers indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return (player == obj);
    }];
    if (playerIndex != NSNotFound) {
        NSMutableArray* activePlayersMutable = [_activePlayers mutableCopy];
        [activePlayersMutable removeObjectAtIndex:playerIndex];
        _activePlayers = activePlayersMutable;
    }
}

@end
