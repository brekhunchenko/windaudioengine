//
//  MMQueuePlayer.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMQueuePlayer.h"
#import "MMPlayerItem.h"
#import "MMPlayer.h"
#import "MMPlaybackTimeInterval.h"

@interface MMQueuePlayer()

@property (nonatomic, strong) NSArray<MMPlayerItem *> *sheduledPlayerItems;

@property (nonatomic, strong) id boundaryTimeObserver;

@property (nonatomic, assign) BOOL paused;

@end

@implementation MMQueuePlayer

#pragma mark - Initializers

- (instancetype)initWithPlayerItems:(NSArray<MMPlayerItem *> *)playerItems {
    self = [super init];
    if (self) {
        _paused = NO;
        _playerItems = playerItems;
        _sheduledPlayerItems = playerItems;
    }
    return self;
}

#pragma mark - Overridden

- (void)play {
    if (_paused == NO) {
        if ([self _havePlayerItemsInQueue]) {
            [self _replaceCurrentItemWithNextQueueItem];
            [super play];
        } else {
            if ([self.delegate respondsToSelector:@selector(queuePlayerDidReachEndOfPlayback:)]) {
                [self.delegate queuePlayerDidReachEndOfPlayback:self];
            }
        }
    } else {
        [super pause];
        _paused = NO;
    }
}

- (void)pause {
    [super pause];
    _paused = YES;
}

- (void)stop {
    [self pause];
    
    _sheduledPlayerItems = _playerItems;
    [self _replaceCurrentItemWithNextQueueItem];
    for (MMPlayerItem* playerItem in _sheduledPlayerItems) {
        if (playerItem.playbackTimeInterval) {
            [playerItem seekToTime:CMTimeMakeWithSeconds(playerItem.playbackTimeInterval.startTime, NSEC_PER_SEC)];
        }
    }
    
    if (_boundaryTimeObserver) {
        [self removeTimeObserver:_boundaryTimeObserver];
    }
    
    if ([self.delegate respondsToSelector:@selector(queuePlayerDidReachEndOfPlayback:)]) {
        [self.delegate queuePlayerDidReachEndOfPlayback:self];
    }
}

- (BOOL)isPaused {
    return _paused;
}

#pragma mark - Helpers

- (BOOL)_havePlayerItemsInQueue {
    return (_sheduledPlayerItems.count > 0);
}

- (void)_replaceCurrentItemWithNextQueueItem {
    if ([self _havePlayerItemsInQueue]) {
        NSMutableArray* playerItemsMutable = [_sheduledPlayerItems mutableCopy];
        [playerItemsMutable removeObjectAtIndex:0];
        MMPlayerItem* currentPlayerItem = [_sheduledPlayerItems objectAtIndex:0];
        _sheduledPlayerItems = playerItemsMutable;
        
        [self replaceCurrentItemWithPlayerItem:currentPlayerItem];

        if (_boundaryTimeObserver) {
            [self removeTimeObserver:_boundaryTimeObserver];
        }
        __weak typeof(self) weakSelf = self;
        if ([(MMPlayerItem *)self.currentItem playbackTimeInterval]) {
            CMTime observingTime = CMTimeMakeWithSeconds([(MMPlayerItem *)self.currentItem playbackTimeInterval].endTime, NSEC_PER_SEC);
            _boundaryTimeObserver = [self addBoundaryTimeObserverForTimes:@[[NSValue valueWithCMTime:observingTime]]
                                                                    queue:NULL
                                                               usingBlock:^{
                                                                   if ([weakSelf _havePlayerItemsInQueue]) {
                                                                       [weakSelf _replaceCurrentItemWithNextQueueItem];
                                                                   } else {
                                                                       if ([weakSelf.delegate respondsToSelector:@selector(queuePlayerDidReachEndOfPlayback:)]) {
                                                                           [weakSelf.delegate queuePlayerDidReachEndOfPlayback:weakSelf];
                                                                       }
                                                                   }
                                                               }];
        }
    }
}

@end
