//
//  MMAudioMicrophone.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@protocol MMAudioMicrophoneDelegate;

@interface MMAudioMicrophone : NSObject

- (instancetype)init; 

@property (nonatomic, assign, getter=isMicrophoneEbabled) BOOL enableMicrophone;
@property (nonatomic, weak) id<MMAudioMicrophoneDelegate> delegate;

- (AudioStreamBasicDescription)audioStreamBasicDescription;

@end

@protocol MMAudioMicrophoneDelegate <NSObject>

- (void)audioMicrophone:(MMAudioMicrophone *)microphone
     recievedBufferList:(AudioBufferList *)bufferList
         withBufferSize:(UInt32)bufferSize;

@end
