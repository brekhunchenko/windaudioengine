//
//  AppDelegate.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


#pragma mark - UIApplication

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self _setupAudioSession];
    
    return YES;
}

#pragma mark - Setup

- (void)_setupAudioSession {
    NSError* error = nil;
    [[AVAudioSession sharedInstance] setPreferredSampleRate:48000 error:&error];
    if (error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }
}

@end
