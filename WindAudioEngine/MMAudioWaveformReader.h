//
//  MMAudioWaveformReader.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/22/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMPlaybackTimeInterval;

@interface MMAudioWaveformReader : NSObject

- (instancetype)initWithURL:(NSURL *)fileURL;

@property (nonatomic, strong, readonly) NSURL *fileURL;

- (NSData *)readWaveformDataAtTimeInterval:(MMPlaybackTimeInterval *)timeInterval;
- (NSTimeInterval)audioDuration;

@end
