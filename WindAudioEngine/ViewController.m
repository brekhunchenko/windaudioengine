//
//  ViewController.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "ViewController.h"
#import "MMAudioMicrophone.h"
#import "MMAudioRecorder.h"
#import "MMAudioEngine.h"
#import "MMPlayer.h"
#import "MMPlaybackTimeInterval.h"
#import "MMPlayerItem.h"
#import "MMQueuePlayer.h"
#import "MMAudioWaveformReader.h"
#import "WaveformDebugView.h"

#import <AVFoundation/AVFoundation.h>
#import <MBProgressHUD.h>
#import <TTRangeSlider.h>

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface ViewController () < MMAudioMicrophoneDelegate >

@property (weak, nonatomic) IBOutlet UIButton *playRecordedSoundButton;
@property (weak, nonatomic) IBOutlet UISwitch *recordSwitch;
@property (weak, nonatomic) IBOutlet WaveformDebugView *waveformDebugView;
@property (weak, nonatomic) IBOutlet TTRangeSlider *rangeSlider;

@property (nonatomic, strong) MMAudioMicrophone* microphone;
@property (nonatomic, strong) MMAudioRecorder* recorder;

@property (nonatomic, strong) AVPlayer* player;

@end

@implementation ViewController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    [self _setupAudioSession];
    [self _setupMicrophone];
    [self rangeSliderDidFinishChanginValue:_rangeSlider];
}

#pragma mark - Setup

- (void)_setupAudioSession {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error) {
        NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
    }
    [session setActive:YES error:&error];
    if (error) {
        NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
    }
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&sessionError];
    if (error) {
        NSLog(@"Error setting up audio session option: %@", error.localizedDescription);
    }
}

- (void)_setupMicrophone {
    _microphone = [[MMAudioMicrophone alloc] init];
    _microphone.delegate = self;
}

#pragma mark - Actions

- (IBAction)recordingSwitchValueChanged:(UISwitch *)sender {
    if ([sender isOn]) {
        [self _beginNewAudioFileRecording];
    } else {
        [self _finishAudioFileRecording];
    }
}

- (IBAction)playRecordedSoundButtonAction:(id)sender {
    [self _finishAudioFileRecording];
    
    NSURL* fileURL = _recorder.fileURL;
    AVAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:asset];
    _player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    [_player play];
}

- (IBAction)playAudioButtonAction:(id)sender {
    MMPlaybackTimeInterval* playbackTimeInterval = [MMPlaybackTimeInterval playbackTimeIntervalWithSecondsFromTime:31.0f
                                                                                                           seconds:3.0f];
    MMPlayerItem* playerItem = [[MMPlayerItem alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"track_test_3" withExtension:@"m4a"]
                                            playbackTimeInterval:playbackTimeInterval];
    [[MMAudioEngine sharedInstance] startPlaybackForPlayerItem:playerItem];
}

- (IBAction)playPlaylistOfExcerpts:(id)sender {
    MMPlaybackTimeInterval* playbackTimeInterval_0 = [MMPlaybackTimeInterval playbackTimeIntervalWithSecondsFromTime:31.0f
                                                                                                             seconds:2.0f];
    MMPlayerItem* playerItem_0 = [[MMPlayerItem alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"track_test_0" withExtension:@"m4a"]
                                            playbackTimeInterval:playbackTimeInterval_0];

    MMPlaybackTimeInterval* playbackTimeInterval_1 = [MMPlaybackTimeInterval playbackTimeIntervalWithStartTime:11.0f
                                                                                                       endTime:16.0f];
    MMPlayerItem* playerItem_1 = [[MMPlayerItem alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"track_test_1" withExtension:@"m4a"]
                                            playbackTimeInterval:playbackTimeInterval_1];
    
    
    [[MMAudioEngine sharedInstance] startPlaybackForPlaylistWithItems:@[playerItem_0, playerItem_1]];
}

- (IBAction)rangeSliderDidFinishChanginValue:(TTRangeSlider *)sender {
    float selectedMin = sender.selectedMinimum;
    float selectedMax = sender.selectedMaximum;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSURL* fileURL = [[NSBundle mainBundle] URLForResource:@"track_test_1" withExtension:@"m4a"];
        MMAudioWaveformReader* audioWaveformReader = [[MMAudioWaveformReader alloc] initWithURL:fileURL];
        NSTimeInterval playbackStartTime = (selectedMin/100.0f)*[audioWaveformReader audioDuration];
        NSTimeInterval playbackEndTime = (selectedMax/100.0f)*[audioWaveformReader audioDuration];
        MMPlaybackTimeInterval* timeInterval = [MMPlaybackTimeInterval playbackTimeIntervalWithStartTime:playbackStartTime
                                                                                                 endTime:playbackEndTime];
        NSData* waveformData = [audioWaveformReader readWaveformDataAtTimeInterval:timeInterval];
      
        dispatch_async(dispatch_get_main_queue(), ^{
            [_waveformDebugView setWaveformData:waveformData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

- (IBAction)switchPlayerButtonAction:(id)sender {
  MMPlaybackTimeInterval* playbackTimeInterval_0 = [MMPlaybackTimeInterval playbackTimeIntervalWithSecondsFromTime:31.0f
                                                                                                           seconds:15.0f];
  MMPlayerItem* playerItem_0 = [[MMPlayerItem alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"track_test_0" withExtension:@"m4a"]
                                            playbackTimeInterval:playbackTimeInterval_0];
  [[MMAudioEngine sharedInstance] startPlaybackForPlayerItem:playerItem_0];
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    MMPlaybackTimeInterval* playbackTimeInterval_1 = [MMPlaybackTimeInterval playbackTimeIntervalWithStartTime:11.0f
                                                                                                       endTime:16.0f];
    MMPlayerItem* playerItem_1 = [[MMPlayerItem alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"track_test_1" withExtension:@"m4a"]
                                              playbackTimeInterval:playbackTimeInterval_1];

    [[MMAudioEngine sharedInstance] switchActivePlayerItem:playerItem_0
                                                  withItem:playerItem_1];
  });
}

#pragma mark - MMAudioMicrophone Delegate

- (void)audioMicrophone:(MMAudioMicrophone *)microphone
     recievedBufferList:(AudioBufferList *)bufferList
         withBufferSize:(UInt32)bufferSize {
    [self.recorder appendBufferList:bufferList
                     withBufferSize:bufferSize];
}

#pragma mark - Helpers

- (void)_beginNewAudioFileRecording {
    NSURL* testFileURL = [self _createTestFilePathURL];
    _recorder = [[MMAudioRecorder alloc] initWithURL:testFileURL
                                        clientFormat:_microphone.audioStreamBasicDescription];
    NSLog(@"Testing file URL: %@", testFileURL.path);
    _microphone.enableMicrophone = YES;
    
    [_recordSwitch setOn:YES];
    _playRecordedSoundButton.userInteractionEnabled = YES;
    _playRecordedSoundButton.alpha = 1.0f;
}

- (void)_finishAudioFileRecording {
    [_recorder closeFileRecording];
    _microphone.enableMicrophone = NO;
    
    [_recordSwitch setOn:NO];
}

- (NSURL *)_createTestFilePathURL {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", basePath, [NSString stringWithFormat:@"%@.m4a", [NSUUID UUID].UUIDString]]];
}

@end
