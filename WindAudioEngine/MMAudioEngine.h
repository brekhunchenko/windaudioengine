//
//  MMAudioEngine.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMPlayerProtocol.h"

@class MMPlayerItem, MMPlayer;

@interface MMAudioEngine : NSObject

+ (instancetype)sharedInstance;

- (void)startPlaybackForPlayerItem:(MMPlayerItem *)playerItem;
- (void)startPlaybackForPlaylistWithItems:(NSArray<MMPlayerItem *> *)playerItems;
- (void)switchActivePlayerItem:(MMPlayerItem *)currentlyPlayingItem withItem:(MMPlayerItem *)playerItem;

- (id <MMPlayerProtocol>)playerWithItem:(MMPlayerItem *)playerItem;
@property (nonatomic, strong, readonly) NSArray<id <MMPlayerProtocol>> *activePlayers;

@end
