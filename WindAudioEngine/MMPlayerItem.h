//
//  MMPlayerItem.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@class MMPlaybackTimeInterval;

@interface MMPlayerItem : AVPlayerItem

- (instancetype)initWithURL:(NSURL *)URL
       playbackTimeInterval:(MMPlaybackTimeInterval *)playbackTimeInterval;

@property (nonatomic, strong, readonly) NSURL* URL;
@property (nonatomic, strong, readonly) MMPlaybackTimeInterval *playbackTimeInterval;

- (BOOL)isEqualToPlayerItem:(MMPlayerItem *)playerItem;

@end
