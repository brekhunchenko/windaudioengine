//
//  WaveformDebugView.h
//  TXTMIDIRecorder
//
//  Created by Yaroslav on 8/3/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaveformDebugView : UIView

@property (nonatomic, strong) NSData* waveformData;

@end
