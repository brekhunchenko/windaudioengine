//
//  MMQueuePlayer.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "MMPlayerProtocol.h"

@class MMPlayerItem;

@protocol MMQueuePlayerDelegate;

@interface MMQueuePlayer : AVPlayer < MMPlayerProtocol >

- (instancetype)initWithPlayerItems:(NSArray<MMPlayerItem *> *)playerItems;

@property (nonatomic, strong, readonly) NSArray<MMPlayerItem *> *playerItems;
@property (nonatomic, weak) id<MMQueuePlayerDelegate> delegate;

@end

@protocol MMQueuePlayerDelegate <NSObject>

- (void)queuePlayerDidReachEndOfPlayback:(MMQueuePlayer *)player;

@end
