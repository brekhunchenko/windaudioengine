//
//  MMPlayerItem.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMPlayerItem.h"
#import "MMPlaybackTimeInterval.h"

@implementation MMPlayerItem

#pragma mark - Initializers

- (instancetype)initWithURL:(NSURL *)URL
       playbackTimeInterval:(MMPlaybackTimeInterval *)playbackTimeInterval {
    self = [super initWithURL:URL];
    if ([self _isValidPlaybackTimeInterval:playbackTimeInterval] == NO) {
        NSLog(@"Invalid playback time interval.");
        return nil;
    }
    
    if (self) {
        _URL = URL;
        _playbackTimeInterval = playbackTimeInterval;
        if (_playbackTimeInterval) {
            [self seekToTime:CMTimeMakeWithSeconds(_playbackTimeInterval.startTime, NSEC_PER_SEC)];
        }
    }
    return self;
}

#pragma mark - Helpers

- (BOOL)_isValidPlaybackTimeInterval:(MMPlaybackTimeInterval *)playbackTimeInterval {
    if (playbackTimeInterval.startTime >= playbackTimeInterval.endTime) {
        return NO;
    }
    if (CMTimeGetSeconds(self.asset.duration) < playbackTimeInterval.endTime) {
        return NO;
    }
    return YES;
}

#pragma mark - Class Methods

- (BOOL)isEqualToPlayerItem:(MMPlayerItem *)playerItem {
    BOOL URLsEqual = [_URL.path isEqualToString:playerItem.URL.path];
    BOOL playbackTimeIntervalEqual = (_playbackTimeInterval.startTime == playerItem.playbackTimeInterval.startTime)
                                   && (_playbackTimeInterval.endTime == playerItem.playbackTimeInterval.endTime);
    return URLsEqual && playbackTimeIntervalEqual;
}

@end
