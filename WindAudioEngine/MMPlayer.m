//
//  MMPlayer.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMPlayer.h"
#import "MMPlaybackTimeInterval.h"
#import "MMPlayerItem.h"

@interface MMPlayer()

@property (nonatomic, strong) id boundaryTimeObserver;
@property (nonatomic, strong) id periodicTimeObserver;

@property (nonatomic, assign) BOOL paused;

@end

@implementation MMPlayer

#pragma mark - Initializers

- (instancetype)initWithPlayerItem:(MMPlayerItem *)playerItem {
    if (playerItem == nil || ([playerItem isMemberOfClass:[MMPlayerItem class]] == NO)) {
        NSLog(@"Invalid player item.");
        return nil;
    }
    
    self = [super initWithPlayerItem:playerItem];
    if (self) {
        _paused = NO;
        
        if (playerItem.playbackTimeInterval == nil) {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(itemDidFinishPlayingNotification:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:playerItem];
        }
    }
    return self;
}

#pragma mark - 

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Custom Setters & Getters 

- (MMPlayerItem *)playerItem {
  return (MMPlayerItem *)self.currentItem;
}

#pragma mark - Overridden

- (void)play {
    [super play];
    
    if (_paused == NO) {
        __weak typeof(self) weakSelf = self;
        if ([(MMPlayerItem *)self.currentItem playbackTimeInterval]) {
            CMTime observingTime = CMTimeMakeWithSeconds([(MMPlayerItem *)self.currentItem playbackTimeInterval].endTime, NSEC_PER_SEC);
            _boundaryTimeObserver = [self addBoundaryTimeObserverForTimes:@[[NSValue valueWithCMTime:observingTime]]
                                                                    queue:NULL
                                                               usingBlock:^{
               [weakSelf pause];
                                                                   
               [weakSelf _finishPlayback];
            }];
        }
        
        _periodicTimeObserver = [self addPeriodicTimeObserverForInterval:self.currentItem.asset.duration
                                                                   queue:NULL
                                                              usingBlock:^(CMTime time) {
            if ([weakSelf.delegate respondsToSelector:@selector(player:didUpdateTime:)]) {
                [weakSelf.delegate player:weakSelf
                            didUpdateTime:weakSelf.currentItem.currentTime];
            }
        }];
    }
}

- (void)pause {
    _paused = YES;
    [super pause];
}

- (BOOL)isPaused {
    return _paused;
}

- (void)stop {
    [self pause];
    
    MMPlaybackTimeInterval *playbackTimeInterval = [(MMPlayerItem *)self.currentItem playbackTimeInterval];
    if (playbackTimeInterval) {
        [self seekToTime:CMTimeMakeWithSeconds(playbackTimeInterval.startTime, NSEC_PER_SEC)];
    }
    
    [self _finishPlayback];
}

#pragma mark - Notifications 

- (void)itemDidFinishPlayingNotification:(NSNotification *)notification {
    [self _finishPlayback];
}

#pragma mark - Helpers

- (void)_finishPlayback {
    if (_boundaryTimeObserver) {
        [self removeTimeObserver:_boundaryTimeObserver];
    }
    if (_periodicTimeObserver) {
        [self removeTimeObserver:_periodicTimeObserver];
    }
    
    if ([self.delegate respondsToSelector:@selector(playerDidReachEndOfPlayback:)]) {
        [self.delegate playerDidReachEndOfPlayback:self];
    }
}

@end
