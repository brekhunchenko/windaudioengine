//
//  MMAudioRecorder.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MMAudioRecorder : NSObject

- (instancetype)initWithURL:(NSURL *)fileURL
               clientFormat:(AudioStreamBasicDescription)clientFormat;

@property (nonatomic, strong, readonly) NSURL* fileURL;

- (void)appendBufferList:(AudioBufferList *)bufferList
          withBufferSize:(UInt32)bufferSize;

- (void)closeFileRecording;

@end
