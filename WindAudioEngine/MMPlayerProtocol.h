//
//  MMPlayerProtocol.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/19/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMPlayerProtocol <NSObject>

- (void)play;
- (void)stop;
- (void)pause;
- (BOOL)isPaused;

@end
