//
//  MMPlayer.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "MMPlayerProtocol.h"

@class MMPlayerItem;

@protocol MMPlayerDelegate;

@interface MMPlayer : AVPlayer < MMPlayerProtocol >

- (instancetype)initWithPlayerItem:(MMPlayerItem *)playerItem NS_DESIGNATED_INITIALIZER;
@property (nonatomic, strong, readonly) MMPlayerItem* playerItem;

@property (nonatomic, weak) id<MMPlayerDelegate> delegate;

@end

@protocol MMPlayerDelegate <NSObject>

@optional
- (void)player:(MMPlayer *)player didUpdateTime:(CMTime)time;
- (void)playerDidReachEndOfPlayback:(MMPlayer *)player;

@end
