//
//  MMAudioRecorder.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMAudioRecorder.h"
#import <AVFoundation/AVFoundation.h>

#import "MMAudioUtils.h"

typedef struct {
    AudioFileTypeID audioFileTypeID;
    ExtAudioFileRef extAudioFileRef;
    AudioStreamBasicDescription clientFormat;
    BOOL fileRecordingClosed;
    AudioStreamBasicDescription fileFormat;
} MMAudioRecorderInfo;

@interface MMAudioRecorder()

@property (nonatomic, assign) MMAudioRecorderInfo *recorderInfo;

@end

@implementation MMAudioRecorder

#pragma mark - Initializers

- (instancetype)initWithURL:(NSURL *)fileURL
               clientFormat:(AudioStreamBasicDescription)clientFormat {
    self = [super init];
    if (self) {
        _fileURL = fileURL;
        
        self.recorderInfo = (MMAudioRecorderInfo *)calloc(1, sizeof(MMAudioRecorderInfo));
        self.recorderInfo->audioFileTypeID = kAudioFileM4AType;
        AudioStreamBasicDescription fileFormat;
        memset(&fileFormat, 0, sizeof(fileFormat));
        fileFormat.mFormatID = kAudioFormatMPEG4AAC;
        fileFormat.mChannelsPerFrame = clientFormat.mChannelsPerFrame;
        fileFormat.mSampleRate = clientFormat.mSampleRate;
        self.recorderInfo->clientFormat = clientFormat;
        self.recorderInfo->fileFormat = fileFormat;
        
        UInt32 propSize = sizeof(self.recorderInfo->fileFormat);
        [MMAudioUtils checkStatus:AudioFormatGetProperty(kAudioFormatProperty_FormatInfo,
                                                         0,
                                                         NULL,
                                                         &propSize,
                                                         &self.recorderInfo->fileFormat)
                          message:"Get kAudioFormatProperty_FormatInfo failed."];
        
        [MMAudioUtils checkStatus:ExtAudioFileCreateWithURL((__bridge CFURLRef)_fileURL,
                                                            self.recorderInfo->audioFileTypeID,
                                                            &self.recorderInfo->fileFormat,
                                                            NULL,
                                                            kAudioFileFlags_EraseFile,
                                                            &self.recorderInfo->extAudioFileRef)
                          message:"Failed to create audio file"];
        
        [MMAudioUtils checkStatus:ExtAudioFileSetProperty(self.recorderInfo->extAudioFileRef,
                                                          kExtAudioFileProperty_ClientDataFormat,
                                                          sizeof(self.recorderInfo->clientFormat),
                                                          &self.recorderInfo->clientFormat)
                          message:"Failed to set client format on recorded audio file"];
        self.recorderInfo->clientFormat = self.recorderInfo->clientFormat;
    }
    return self;
}

#pragma mark -

- (void)dealloc {
    if (self.recorderInfo->fileRecordingClosed == NO) {
        [self closeFileRecording];
    }
    free(self.recorderInfo);
}

#pragma mark - Class Methods

- (void)appendBufferList:(AudioBufferList *)bufferList withBufferSize:(UInt32)bufferSize {
    if (self.recorderInfo->fileRecordingClosed == NO) {
        [MMAudioUtils checkStatus:ExtAudioFileWrite(self.recorderInfo->extAudioFileRef,
                                                    bufferSize,
                                                    bufferList)
                          message:"Failed to write audio data to recorded audio file"];
    }
}

- (void)closeFileRecording {
    if (self.recorderInfo->fileRecordingClosed == NO) {
        [MMAudioUtils checkStatus:ExtAudioFileDispose(self.recorderInfo->extAudioFileRef)
                          message:"Failed to close audio file"];
        self.recorderInfo->fileRecordingClosed = YES;
    }
}

@end
